import * as d3 from 'd3';
import { Component } from '@angular/core';
// import { MatSelectModule } from '@angular/material/select';
// import { Papa } from 'ngx-papaparse';
// import * as myFile from '../assets/data2.csv';
// import { HttpClient } from '@angular/common/http'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Visualization Test';
}
