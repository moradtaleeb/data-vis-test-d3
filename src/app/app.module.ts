import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { AppComponent } from './app.component';
import { VisComponent } from './vis.component';
import { Vis2Component } from './vis2.component';
import { RouterModule, Routes } from '@angular/router';

const appRoutes: Routes = [
  { path: 'v2', component: Vis2Component },
  { path: 'v1',      component: VisComponent },
];

@NgModule({
  declarations: [
    AppComponent,
    VisComponent,
    Vis2Component
  ],
  imports: [
    BrowserModule,
    FormsModule,
    RouterModule.forRoot(
      appRoutes
    )
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
