import * as d3 from 'd3';
import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './vis.component.html',
  styleUrls: ['./vis.component.css']
})
export class VisComponent {
  title = 'Vis';
  location = 'Arizona';
  dataToVis = 'Unemployment';

  // from ngModel get Location/s and dataToVis
  constructor() {
    this.getData(this.location, this.dataToVis);
    // var states = [];
    // // fill states options
    // d3.csv("../assets/data2.csv", function(d) {
    //   return d.State;
    // }).then(function(data) {
    //   for (let entry of data) {
    //     console.log(entry);
    //     if(states.indexOf(entry) === -1) {
    //       states.push(entry);
    //     }
    //   }
    // }

    // console.log( states );
  }

  drawCharts() {
    this.getData(this.location, this.dataToVis);
  }

  // #1 filter data
  getData(state_value, column_name) { /// locations, dataToVis

    var filteredData = [];
    // var counties = [];

    d3.csv("../assets/data2.csv", function(d) {
      return {
        id: d.CensusId,
        state: d.State,
        county: d.County,
        value: +d[column_name]
      };
    }).then(function(data) {
      // console.log(data);
      for (let entry of data) {
        // console.log(entry.state);
        if ( entry.state == state_value) { /// if entry.state in locations && entry.county in locations
          filteredData.push({ county_name: entry.county, county_value: entry.value });
          // filteredData.val = entry.val

          // filteredData.push(entry.value);
          // counties.push(entry.county);
        }
      }

      // console.log(filteredData[0].county_name);
      // console.log(counties);

      const color = d3.scaleOrdinal(["#66c2a5","#fc8d62","#8da0cb",
           "#e78ac3","#a6d854","#ffd92f","#1abc9c","#2ecc71","#3498db",
           "#9b59b6","#34495e","#f1c40f","#e67e22","#e74c3c","#95a5a6",
           "#bdc3c7","#c0392b","#d35400","#f39c12","#8e44ad","#16a085",
           "#6D214F","##FC427B","#55E6C1","#FEA47F","#EAB543","#D6A2E8"]);

      //
      /// Bar Chart
      //
      d3.select("svg").selectAll("*").remove();

      var svg = d3.select("svg"),
      margin = {
        top: 20,
        right: 20,
        bottom: 30,
        left: 50
      },
      width = +svg.attr("width") - margin.left - margin.right,
      height = +svg.attr("height") - margin.top - margin.bottom,
      g = svg.append("g").attr("transform", "translate(" + margin.left + "," + margin.top + ")");

      var x = d3.scaleBand()
        .rangeRound([0, width])
        .padding(0.1);

      var y = d3.scaleLinear()
        .rangeRound([height, 0]);

      x.domain(filteredData.map(function (d) {
        return d.county_name;
      }));
      y.domain([0, d3.max(filteredData, function (d) {
        return Number(d.county_value);
      })]);

      g.append("g")
      .attr("transform", "translate(0," + height + ")")
      .call(d3.axisBottom(x))

      g.append("g")
      .call(d3.axisLeft(y))
      .append("text")
      .attr("fill", "#000")
      .attr("transform", "rotate(-90)")
      .attr("y", 6)
      .attr("dy", "0.71em")
      .attr("text-anchor", "end")
      .text("Unemployment");

      g.selectAll(".bar")
      .data(filteredData)
      .enter().append("rect")
      .attr("class", "bar")
      .attr("x", function (d) {
        // console.log(d);
        return x(d.county_name);
      })
      .attr("y", function (d) {
        return y(Number(d.county_value));
      })
      .attr("fill", (d, i) => color(i))
      .attr("width", x.bandwidth())
      .attr("height", function (d) {
        return height - y(Number(d.county_value));
      });


      // donut chart
      const dwidth = 540;
      const dheight = 540;
      const radius = Math.min(dwidth, dheight) / 2;

      d3.select("#chart-area").selectAll("*").remove();

      const svg2 = d3.select("#chart-area")
        .append("svg")
            .attr("width", dwidth)
            .attr("height", dheight)
        .append("g")
            .attr("transform", `translate(${dwidth / 2}, ${dheight / 2})`);

      const pie = d3.pie()
          .value(d => d.county_value)
          .sort(null);

      const arc = d3.arc()
          .innerRadius(0)
          .outerRadius(radius);

      function type(d) {
        d.county_value = Number(d.county_value);
        return d;
      }

      function arcTween(a) {
        const i = d3.interpolate(this._current, a);
        this._current = i(1);
        return (t) => arc(i(t));
      }

      const path = svg2.selectAll("path")
          .data(pie(filteredData));

      // Update existing arcs
      path.transition().duration(200).attrTween("d", arcTween);

      // Enter new arcs
      /// random color: '#'+Math.floor(Math.random()*16777215).toString(16)
      path.enter().append("path")
          .attr("fill", (d, i) => color(i))
          .attr("d", arc)
          .attr("stroke", "white")
          .attr("stroke-width", "6px")
          .each(function(d) { this._current = d; });

      
      d3.select("#pie-keys").append("g").attr("height","auto").selectAll(".key")
        .data(filteredData)
        .enter().append("text")
          // .attr("fill",5)
          .attr("transform", (d, i) => "translate(" + 10 + "," + i*20 + ")")
          .text((d) => d.county_name)
          .attr("fill", (d, i) => color(i))
          // .append("text")
          //   .attr()

      // .attr("x", function (d) {
      //   // console.log(d);
      //   return x(d.county_name);
      // })


    });

  }

}
